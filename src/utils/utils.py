
import math
import numpy as np

def calcImgSize(im, options, conf, matSize):
    imRatio = im.size[0] / im.size[1]
    width = 0
    height = 0
    ratio = 1

    # ratio
    if options.ratio:
        ratio = options.ratio
    elif "ratio" in conf:
        ratio = conf["ratio"]
    # alter ratio based on subchar matrix
    subcharMode = options.subchar is not None
    if subcharMode:
        matRatio = matSize[0] / matSize[1]
        ratio *= matRatio

    # if pixels option is specified
    if options.pixels:
        realRatio = im.size[0] * ratio / im.size[1]
        width = math.sqrt(realRatio * options.pixels)
        size = (math.floor(width), math.floor((width/realRatio)))
        return size

    # width
    if options.width:
        width = options.width
    elif not options.height and "width" in conf:
        width = conf["width"]

    # height
    if options.height:
        height = options.height
    elif not options.width and "height" in conf:
        height = conf["height"]

    # apply ratio
    if width == 0:
        width = int((height * imRatio) * ratio)
    if height == 0:
        height = int((width /imRatio) / ratio)

    return (width, height)

def hexColtoTuple(color):
    if color[0] == '#':
        color = color[1:]
    color = color.upper()
    for i in color:
        if not (ord(i) > 64 and ord(i) < 71) and not (ord(i) > 47 and ord(i) < 58):
            raise ValueError('Given string is not in HEX color format.')
    if len(color) == 3:
        t = (int(color[0] + color[0], 16), int(color[1] + color[1], 16), int(color[2] + color[2], 16), 255)
    elif len(color) == 6:
        t = (int(color[0:2], 16), int(color[2:4], 16), int(color[4:6], 16), 255)
    else:
        raise ValueError('Given string is not in HEX color format.')
    return t


def parseMatSize(matSizeStr):
    matSize = np.array(matSizeStr.split(','), dtype=int)
    if len(matSize) != 2:
        raise Exception("matSize format not understood. Must be 'x,y'.")
    return matSize