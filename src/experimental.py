import sys
import os
import json
from optparse import OptionParser, OptionGroup, TitledHelpFormatter, IndentedHelpFormatter
import numpy as np

import chars
import img
import utils

def dispTpeg(charWMat):
    for line in charWMat:
        for charW in line:
            print(charW[0], end='', sep='')
        print()

if __name__ == "__main__":

    imPath = '/home/revetoon/Downloads/star4.png'

    with open(os.path.join(os.path.dirname(__file__), 'config.json'), 'r') as config:
        conf = config.read()
        conf = json.loads(conf)
    # Load font
    font = chars.loadFont(os.path.join(os.path.dirname(__file__), conf["font"]), 15)
    # test
    # mat = chars.charWeightMatrix('⣿', font, (2, 2))

    characters = ' ⠁⠂⠃⠄⠅⠆⠇⠈⠉⠊⠋⠌⠍⠎⠏⠐⠑⠒⠓⠔⠕⠖⠗⠘⠙⠚⠛⠜⠝⠞⠟⠠⠡⠢⠣⠤⠥⠦⠧⠨⠩⠪⠫⠬⠭⠮⠯⠰⠱⠲⠳⠴⠵⠶⠷⠸⠹⠺⠻⠼⠽⠾⠿⡀⡁⡂⡃⡄⡅⡆⡇⡈⡉⡊⡋⡌⡍⡎⡏⡐⡑⡒⡓⡔⡕⡖⡗⡘⡙⡚⡛⡜⡝⡞⡟⡠⡡⡢⡣⡤⡥⡦⡧⡨⡩⡪⡫⡬⡭⡮⡯⡰⡱⡲⡳⡴⡵⡶⡷⡸⡹⡺⡻⡼⡽⡾⡿⢀⢁⢂⢃⢄⢅⢆⢇⢈⢉⢊⢋⢌⢍⢎⢏⢐⢑⢒⢓⢔⢕⢖⢗⢘⢙⢚⢛⢜⢝⢞⢟⢠⢡⢢⢣⢤⢥⢦⢧⢨⢩⢪⢫⢬⢭⢮⢯⢰⢱⢲⢳⢴⢵⢶⢷⢸⢹⢺⢻⢼⢽⢾⢿⣀⣁⣂⣃⣄⣅⣆⣇⣈⣉⣊⣋⣌⣍⣎⣏⣐⣑⣒⣓⣔⣕⣖⣗⣘⣙⣚⣛⣜⣝⣞⣟⣠⣡⣢⣣⣤⣥⣦⣧⣨⣩⣪⣫⣬⣭⣮⣯⣰⣱⣲⣳⣴⣵⣶⣷⣸⣹⣺⣻⣼⣽⣾⣿'
    # characters = '─━│┃┄┅┆┇┈┉┊┋┌┍┎┏┐┑┒┓└┕┖┗┘┙┚┛├┝┞┟┠┡┢┣┤┥┦┧┨┩┪┫┬┭┮┯┰┱┲┳┴┵┶┷┸┹┺┻┼┽┾┿╀╁╂╃╄╅╆╇╈╉╊╋╌╍╎╏═║╒╓╔╕╖╗╘╙╚╛╜╝╞╟╠╡╢╣╤╥╦╧╨╩╪╫╬╭╮╯╰╱╲╳╴╵╶╷╸╹╺╻╼╽╾╿'

    matSize = (2, 4)

    # char list to matrix
    charsMat = np.array([chars.associateWeightMatrix(i, font, matSize) for i in characters])
    # normalize
    charsMat = chars.normalizeWeightsMat(charsMat)
    # load Img
    im = img.loadImg(imPath)
    imSize = im.size

    # transform image
    im = img.convertImg(im, imSize)

    imMat = np.array(img.getPixels(im))

    if imMat.shape[1] % matSize[0] != 0:
        imMat = np.pad(imMat, ((0, 0), (0, (matSize[0] - (imMat.shape[1] % matSize[0])))), constant_values=(0))
    if imMat.shape[0] % matSize[1] != 0:
        imMat = np.pad(imMat, ((0, matSize[1] - (imMat.shape[0] % matSize[1])), (0, 0)), constant_values=(0))

    del im
    # convert to char w
    charWMat = chars.imgToTpegMat(imMat, charsMat, matSize)

    dispTpeg(charWMat)


