from PIL import Image, ImageOps
import numpy as np

import utils

def loadImg(path):
    img = Image.open(path)
    return img

def setBackground(img, color):
    bg = Image.new('RGBA', img.size, color)
    try:
        img = Image.composite(img, bg, img)
    except ValueError as err:
        raise ValueError("Image not transparent. Cannot set background color.") from err
    return img

def convertImg(img, size, options):
    # set the background if asked
    if options.background:
        bgColor = utils.hexColtoTuple(options.background)
        img = setBackground(img, bgColor)
    # sets the aliasing
    a = Image.NEAREST
    if options.aliasing:
        a = Image.ANTIALIAS
    # resizes
    img = img.resize(size, a)
    # invert colors if asked
    if options.invert:
        img = img.convert('RGB')
        img = ImageOps.invert(img)
    # convert to black and white
    img = img.convert('LA')
    return img

def getPixels(img):
    pix = img.load()
    mat = []
    for y in range(img.size[1]):
        line = []
        for x in range(img.size[0]):
            line.append(pix[x, y][0])
        mat.append(line)
    return mat

def deducePadding(imMat, matSize):
    if imMat.shape[1] % matSize[0] != 0:
        imMat = np.pad(imMat, ((0, 0), (0, (matSize[0] - (imMat.shape[1] % matSize[0])))), constant_values=(0))
    if imMat.shape[0] % matSize[1] != 0:
        imMat = np.pad(imMat, ((0, matSize[1] - (imMat.shape[0] % matSize[1])), (0, 0)), constant_values=(0))
    return imMat