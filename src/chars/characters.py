from PIL import Image, ImageFont, ImageDraw, ImageStat

import numpy as np

# use a TTF font
def loadFont(fontPath, size):
    return ImageFont.truetype(fontPath, size)

def charWeight(char, font):
    # get letter size
    size = font.getsize(char)
    # create img
    image = Image.new("RGB", (size[0], size[1]), "white")
    draw = ImageDraw.Draw(image)
    # draw text
    draw.text((0, 0), char, font=font, fill=256)
    del draw
    # get mean stat
    stats = ImageStat.Stat(image)
    return 255 - stats.mean[0] # Black and white


def associateWeight(char, font):
    return (char, charWeight(char, font))


def weightSorter(charW):
    return charW[1]

def normalizeWeights(charsW):
    minW = charsW[0][1]
    min  = 0
    maxW = charsW[-1][1]
    max  = 255

    reduce = min - minW
    coeff = max / (maxW + reduce)

    normalized = [(charW[0], (charW[1] + reduce) * coeff) for charW in charsW]
    return normalized


def getClosestChar(charsW, num):
    closest = charsW[0]
    for charW in charsW:
        if abs(closest[1] - num) > abs(charW[1] - num):
            closest = charW
    return closest


def inmToTpeg(mat, charsW):
    charWMat = []
    for y in range(len(mat)):
        charWLine = []
        for x in range(len(mat[y])):
            charWLine.append(getClosestChar(charsW, mat[y][x]))
        charWMat.append(charWLine)
    return charWMat

def sanatizeChars(chars, excluded):
    chars = [x for x in chars if not x in excluded]
    return chars


def charWeightMatrix(char, font, matSize = (2, 2)):
    # get letter size
    size = font.getsize(char)
    # create img
    image = Image.new("RGB", (size[0], size[1]), "white")
    draw = ImageDraw.Draw(image)
    # draw text
    draw.text((0, 0), char, font=font, fill=256)
    del draw

    weightMat = []
    mw, mh = matSize

    xCropSize = size[0] / mw
    yCropSize = size[1] / mh

    # image.save('full.bmp')

    for y in range(mh):
        for x in range(mw):
            cropped = image.crop((x * xCropSize, y * yCropSize, (x+1) * xCropSize, (y+1) * yCropSize))
            # cropped.save(str(x) + ' ' + str(y) + '.bmp')
            stats = ImageStat.Stat(cropped)
            weightMat.append(255 - stats.mean[0])
    return np.array(weightMat)



closestMatCache = {}

def getClosestCharMat(Mats, chars, imgMat):
    # check cache
    key = hash(tuple(np.concatenate(imgMat)))
    val = closestMatCache.get(key)
    if val is not None:
        return val

    # simplify imgMat
    imgMat = np.concatenate(imgMat)
    # subsract to all lines
    r = np.subtract(Mats, imgMat)
    r = np.abs(r)
    r = np.sum(r, axis=1)
    best = np.argmin(r)

    # add value to cache
    closestMatCache[key] = chars[best]
    return chars[best]


def imgToTpegMat(img, charMats, matSize = (2, 2)):
    tpeg = []
    mx, my = matSize

    # simplify charMats
    mats = np.concatenate(charMats[:, [1][0]]).reshape(len(charMats), len(charMats[0][1]))
    char = charMats[:, [0][0]]

    for y in range(0, len(img), my):
        charWLine = []
        for x in range(0, len(img[y]), mx):
            limx, limy = x+(mx) ,y+(my)
            imgMat = img[y:limy, x:limx]
            charWLine.append(getClosestCharMat(mats, char, imgMat))
        tpeg.append(charWLine)
    return tpeg

def associateWeightMatrix(char, font, matSize = (2, 2)):
    return [char, charWeightMatrix(char, font, matSize=matSize)]


def normalizeWeightsMat(charsMat):
    normalized = charsMat.copy()
    charMatOnly = normalized[:, 1]
    charMatOnly1d = np.concatenate(charMatOnly)
    minW = np.min(charMatOnly1d)
    min = 0
    maxW = np.max(charMatOnly1d)
    max = 255

    reduce = min - minW
    coeff = max / (maxW + reduce)

    charMatOnly += reduce
    charMatOnly *= coeff

    return normalized

