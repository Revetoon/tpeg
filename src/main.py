#!/usr/bin/env python3

import sys
import os
import json
import numpy as np
from optparse import OptionParser, OptionGroup, TitledHelpFormatter, IndentedHelpFormatter

import chars
import img
import utils



def dispTpeg(charWMat):
    for line in charWMat:
        for charW in line:
            print(charW[0], end='', sep='')
        print()


def parseCL():
    usage = "usage: %prog [options] IMAGE"
    formatter = IndentedHelpFormatter(indent_increment=4, max_help_position=80, width=120)
    parser = OptionParser(usage, formatter=formatter)

    ## CHARSET OPTIONS GROUP
    charsetGroup = OptionGroup(parser, "CHARSET OPTIONS",
                        "Options relative to the specification of a "
                        "set of characters to use in the final render.")

    charsetGroup.add_option("-l", "--letters", dest="letters", type="string",
                      help="gives a list of characters to use in string format")
    charsetGroup.add_option("-L", "--charset", dest="charset", type="string",
                      help="gives a file to extract characters from to use in string format")

    parser.add_option_group(charsetGroup)

    ## SIZE OPTION GROUP
    sizeGroup = OptionGroup(parser, "SIZE OPTIONS",
                        "Options relative to the size of the final render.")

    sizeGroup.add_option("-W", "--width", dest="width", type="int",
                      help="gives the text img width.")
    sizeGroup.add_option("-H", "--height", dest="height", type="int",
                      help="gives the text img height.")
    sizeGroup.add_option("-p", "--pixels", dest="pixels", type="int",
                      help="gives the max number of pixels for the image.")
    sizeGroup.add_option("-r", "--ratio", dest="ratio", type="float",
                      help="gives the height to width ratio.")

    parser.add_option_group(sizeGroup)

    ## IMAGE OPTION GROUP
    imageGroup = OptionGroup(parser, "IMAGE OPTIONS",
                             "Options relative to image operations.")

    imageGroup.add_option("-i", "--invert", dest="invert", action="store_true", default=False,
                      help="inverted colors.")
    imageGroup.add_option("-b", "--background", dest="background", type="string",
                      help="Choose a background color for transparent images. Example: 'FF0000' (red)")
    imageGroup.add_option("-a", "--aliasing", dest="aliasing", action="store_true", default=False,
                      help="Enable image aliasing when resizing.")
    imageGroup.add_option("-s", "--subchar", dest="subchar", type="string",
                      help="Choose subchar mode for rendering 'x,y'. Example : '2,4'")

    parser.add_option_group(imageGroup)

    (options, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("Missing IMAGE path")

    if options.letters and options.charset:
        parser.error("options --letters and --charset are mutually exclusive.")

    if (options.width or options.height) and options.pixels:
        parser.error("option --pixels cannot be set if --width or --height are used.")

    if options.width and options.height and options.ratio:
        parser.error("option --ratio cannot be set if --width and --height are used.")

    return (options, args)





if __name__ == "__main__":
    (options, args) = parseCL()

    # configs
    with open(os.path.join(os.path.dirname(__file__), 'config.json'), 'r') as config:
        conf = config.read()
        conf = json.loads(conf)

    # chars
    if options.charset:
        with open(options.charset, 'r', encoding='utf-8') as charset:
            characters = charset.read()

    elif options.letters:
        characters = options.letters
    else:
        characters = conf["letters"]
        characters = list(set(characters)) # remove duplicates
        characters = chars.sanatizeChars(characters, conf["excluded"]) # removed excluded chars

    if (len(characters) < 2):
        print("Not enough characters. Exiting." ,file=sys.stderr)
        sys.exit(1)

    # check subchar mode
    subcharMatSize = None
    subcharMode = options.subchar is not None
    if subcharMode:
        subcharMatSize = utils.parseMatSize(options.subchar)

    # img
    imPath = args[0]

    # load font
    font = chars.loadFont(os.path.join(os.path.dirname(__file__), conf["font"]), 12)

    charWeightList = None
    if subcharMode:
        # chars , weight tuple
        charWeightList = np.array([chars.associateWeightMatrix(i, font, subcharMatSize) for i in characters])
        # normalize weights
        charWeightList = chars.normalizeWeightsMat(charWeightList)
    else:
        # chars , weight tuple
        charWeightList = [chars.associateWeight(i, font) for i in characters]
        # sort by charWeight
        charWeightList.sort(key=chars.weightSorter, reverse=False)
        # normalize weights
        charWeightList = chars.normalizeWeights(charWeightList)

    # load Img
    im = img.loadImg(imPath)
    imSize = utils.calcImgSize(im, options, conf, subcharMatSize)

    # transform image
    im = img.convertImg(im, imSize, options)

    imMat = np.array(img.getPixels(im))

    if subcharMode:
        # round image shape to fit matSize
        imMat = img.deducePadding(imMat, subcharMatSize)
    del im

    if subcharMode:
        tpeg = chars.imgToTpegMat(imMat, charWeightList, subcharMatSize)
    else:
        # convert to char w
        tpeg = chars.inmToTpeg(imMat, charWeightList)

    dispTpeg(tpeg)

