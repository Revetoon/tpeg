# TPEG

```
100000000000000.·.··.·1000000000111.·.····.·100000000000.······.·.11000000011.··
000000000000000.·.··.·100000000000001·.···.·100000000000.·····.·.00000000000001.
111100000000111.·.··.·100000011000000·..··.·100000001111.····...0000000.10000001
····1000000.········.·1000000.·100000.·.··.·10000001·········.·.0000001·.0000001
...·0000000....·····.·1000000.·000000.·.··.·10000001·········.·.0000001·.0000001
·.··0000000.·.······.·100000011000000·..··.·100000001111·.···.·.0000001·........
·.··0000000.·.······.·100000000000001·.···.·100000000001·.···.·.0000001.00000001
·.··0000000.·.······.·1000000111111.·.····.·100000001111·.···.·.0000001.00000000
·.··0000000.·.······.·1000000.······.·····.·10000001····.····.·.0000001..0000001
·.··0000000.·.······.·1000000.......······.·10000001·········.·.0000001··0000001
·.··1000000.·.······.·1000000.·.··········.·1000000011111·.··...0000000..0000001
·.··0000000.·.······.·1000000.·.··········.·1000000000001·.···.·.000000000000000
·.··1000000.·.······.·1000000.·.··········.·1000000000001·.····.·.11000001.10001
```

**Tpeg** is a python 3 command line program designed to do basic **ASCII art** and even **UNICODE art**.

It uses **PIL** for image operations.  
***
A `config.json` file can be modified to tweak some default options of the program. Command line options
will have the priority over them.  
If the default does not support the characters you want to use, you will need to provide a compatible *ttf* font in the `config.json` file.  
The `charsets/` folder contains premade character sets to use for various displays. You can load them using the `-L` option.

Example run:
```
./src/main.py imgs/Tpeg.png -b FFF -L charsets/matrix.txt -a -W 80
```

### Install

To install the project dependencies, run
```
pip install -r requirements.txt
```

### Usage

```
Usage: main.py [options] IMAGE

Options:
    -h, --help                                  show this help message and exit

    CHARSET OPTIONS:
        Options relative to the specification of a set of characters to use in the final render.

        -l LETTERS, --letters=LETTERS           gives a list of characters to use in string format
        -L CHARSET, --charset=CHARSET           gives a file to extract characters from to use in string format

    SIZE OPTIONS:
        Options relative to the size of the final render.

        -W WIDTH, --width=WIDTH                 gives the text img width.
        -H HEIGHT, --height=HEIGHT              gives the text img height.
        -p PIXELS, --pixels=PIXELS              gives the max number of pixels for the image.
        -r RATIO, --ratio=RATIO                 gives the height to width ratio.

    IMAGE OPTIONS:
        Options relative to image operations.

        -i, --invert                            inverted colors.
        -b BACKGROUND, --background=BACKGROUND  Choose a background color for transparent images. Example: 'FF0000' (red)
        -a, --aliasing                          enable image aliasing when resizing.
        -s SUBCHAR, --subchar=SUBCHAR           Choose subchar mode for rendering 'x,y'. Example : '2,4'

```

### Credits

TheRevetoon